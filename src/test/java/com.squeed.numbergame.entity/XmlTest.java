package com.squeed.numbergame.entity;

import org.junit.Test;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;


public class XmlTest {

    @Test
    public void testXmlFromYr() {
        Serializer serializer = new Persister();
        try {
            WeatherData weatherData = serializer.read(WeatherData.class, getXml());
            System.out.println("weatherData = " + weatherData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getXml() {
        return "<weatherdata>\n" +
                "<location>\n" +
                "<name>Lerum</name>\n" +
                "<type>Administration centre</type>\n" +
                "<country>Sweden</country>\n" +
                "<timezone id=\"Europe/Stockholm\" utcoffsetMinutes=\"120\"/>\n" +
                "<location altitude=\"20\" latitude=\"57.77051\" longitude=\"12.26904\" geobase=\"geonames\" geobaseid=\"2696503\"/>\n" +
                "</location>\n" +
                "<credit>...</credit>\n" +
                "<links>\n" +
                "<link id=\"xmlSource\" url=\"http://www.yr.no/place/Sweden/Västra_Götaland/Lerum/forecast.xml\"/>\n" +
                "<link id=\"xmlSourceHourByHour\" url=\"http://www.yr.no/place/Sweden/Västra_Götaland/Lerum/forecast_hour_by_hour.xml\"/>\n" +
                "<link id=\"overview\" url=\"http://www.yr.no/place/Sweden/Västra_Götaland/Lerum/\"/>\n" +
                "<link id=\"hourByHour\" url=\"http://www.yr.no/place/Sweden/Västra_Götaland/Lerum/hour_by_hour\"/>\n" +
                "<link id=\"longTermForecast\" url=\"http://www.yr.no/place/Sweden/Västra_Götaland/Lerum/long\"/>\n" +
                "</links>\n" +
                "<meta>\n" +
                "<lastupdate>2016-09-09T21:59:00</lastupdate>\n" +
                "<nextupdate>2016-09-10T00:00:00</nextupdate>\n" +
                "</meta>\n" +
                "<sun rise=\"2016-09-09T06:28:57\" set=\"2016-09-09T19:45:52\"/>\n" +
                "<forecast>\n" +
                "<tabular>\n" +
                "<time from=\"2016-09-09T23:00:00\" to=\"2016-09-10T00:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-09T23:00:00 to 2016-09-10T00:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.25\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-09T23:00:00  -->\n" +
                "<windDirection deg=\"166.4\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.4\" name=\"Light air\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T00:00:00\" to=\"2016-09-10T01:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T00:00:00 to 2016-09-10T01:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T00:00:00  -->\n" +
                "<windDirection deg=\"164.2\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.6\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T01:00:00\" to=\"2016-09-10T02:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T01:00:00 to 2016-09-10T02:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T01:00:00  -->\n" +
                "<windDirection deg=\"158.7\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.6\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T02:00:00\" to=\"2016-09-10T03:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T02:00:00 to 2016-09-10T03:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T02:00:00  -->\n" +
                "<windDirection deg=\"162.6\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.7\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T03:00:00\" to=\"2016-09-10T04:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T03:00:00 to 2016-09-10T04:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T03:00:00  -->\n" +
                "<windDirection deg=\"161.3\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.6\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T04:00:00\" to=\"2016-09-10T05:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T04:00:00 to 2016-09-10T05:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T04:00:00  -->\n" +
                "<windDirection deg=\"161.1\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.5\" name=\"Light air\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T05:00:00\" to=\"2016-09-10T06:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T05:00:00 to 2016-09-10T06:00:00 \n" +
                "-->\n" +
                "<symbol number=\"2\" numberEx=\"2\" name=\"Fair\" var=\"mf/02n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T05:00:00  -->\n" +
                "<windDirection deg=\"162.7\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.5\" name=\"Light air\"/>\n" +
                "<temperature unit=\"celsius\" value=\"13\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T06:00:00\" to=\"2016-09-10T07:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T06:00:00 to 2016-09-10T07:00:00 \n" +
                "-->\n" +
                "<symbol number=\"15\" numberEx=\"15\" name=\"Fog\" var=\"15\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T06:00:00  -->\n" +
                "<windDirection deg=\"157.4\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.8\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"13\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T07:00:00\" to=\"2016-09-10T08:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T07:00:00 to 2016-09-10T08:00:00 \n" +
                "-->\n" +
                "<symbol number=\"15\" numberEx=\"15\" name=\"Fog\" var=\"15\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T07:00:00  -->\n" +
                "<windDirection deg=\"156.8\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.9\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"13\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T08:00:00\" to=\"2016-09-10T09:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T08:00:00 to 2016-09-10T09:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T08:00:00  -->\n" +
                "<windDirection deg=\"167.0\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.8\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T09:00:00\" to=\"2016-09-10T10:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T09:00:00 to 2016-09-10T10:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T09:00:00  -->\n" +
                "<windDirection deg=\"178.3\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"1.9\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"16\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T10:00:00\" to=\"2016-09-10T11:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T10:00:00 to 2016-09-10T11:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T10:00:00  -->\n" +
                "<windDirection deg=\"200.5\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"2.7\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T11:00:00\" to=\"2016-09-10T12:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T11:00:00 to 2016-09-10T12:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T11:00:00  -->\n" +
                "<windDirection deg=\"202.7\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"3.2\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"20\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T12:00:00\" to=\"2016-09-10T13:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T12:00:00 to 2016-09-10T13:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T12:00:00  -->\n" +
                "<windDirection deg=\"211.0\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"3.4\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"20\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T13:00:00\" to=\"2016-09-10T14:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T13:00:00 to 2016-09-10T14:00:00 \n" +
                "-->\n" +
                "<symbol number=\"2\" numberEx=\"2\" name=\"Fair\" var=\"02d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T13:00:00  -->\n" +
                "<windDirection deg=\"217.2\" code=\"SW\" name=\"Southwest\"/>\n" +
                "<windSpeed mps=\"3.6\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"21\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T14:00:00\" to=\"2016-09-10T15:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T14:00:00 to 2016-09-10T15:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"01d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T14:00:00  -->\n" +
                "<windDirection deg=\"217.8\" code=\"SW\" name=\"Southwest\"/>\n" +
                "<windSpeed mps=\"3.7\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"22\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T15:00:00\" to=\"2016-09-10T16:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T15:00:00 to 2016-09-10T16:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"01d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T15:00:00  -->\n" +
                "<windDirection deg=\"207.7\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"3.5\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"22\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T16:00:00\" to=\"2016-09-10T17:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T16:00:00 to 2016-09-10T17:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"01d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T16:00:00  -->\n" +
                "<windDirection deg=\"199.2\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"3.6\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"22\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.9\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T17:00:00\" to=\"2016-09-10T18:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T17:00:00 to 2016-09-10T18:00:00 \n" +
                "-->\n" +
                "<symbol number=\"2\" numberEx=\"2\" name=\"Fair\" var=\"02d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T17:00:00  -->\n" +
                "<windDirection deg=\"200.8\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"3.5\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"22\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.7\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T18:00:00\" to=\"2016-09-10T19:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T18:00:00 to 2016-09-10T19:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T18:00:00  -->\n" +
                "<windDirection deg=\"193.7\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"2.8\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"21\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T19:00:00\" to=\"2016-09-10T20:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T19:00:00 to 2016-09-10T20:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T19:00:00  -->\n" +
                "<windDirection deg=\"185.9\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"2.4\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"21\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T20:00:00\" to=\"2016-09-10T21:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T20:00:00 to 2016-09-10T21:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T20:00:00  -->\n" +
                "<windDirection deg=\"170.4\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"1.8\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T21:00:00\" to=\"2016-09-10T22:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T21:00:00 to 2016-09-10T22:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T21:00:00  -->\n" +
                "<windDirection deg=\"154.0\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"1.7\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1014.9\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T22:00:00\" to=\"2016-09-10T23:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T22:00:00 to 2016-09-10T23:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T22:00:00  -->\n" +
                "<windDirection deg=\"155.9\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"2.2\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1014.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-10T23:00:00\" to=\"2016-09-11T00:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-10T23:00:00 to 2016-09-11T00:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.28\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-10T23:00:00  -->\n" +
                "<windDirection deg=\"155.0\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"2.3\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1014.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T00:00:00\" to=\"2016-09-11T01:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T00:00:00 to 2016-09-11T01:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T00:00:00  -->\n" +
                "<windDirection deg=\"157.6\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"2.5\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1014.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T01:00:00\" to=\"2016-09-11T02:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T01:00:00 to 2016-09-11T02:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T01:00:00  -->\n" +
                "<windDirection deg=\"159.9\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"2.7\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1013.9\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T02:00:00\" to=\"2016-09-11T03:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T02:00:00 to 2016-09-11T03:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T02:00:00  -->\n" +
                "<windDirection deg=\"159.3\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"2.8\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"16\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1013.5\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T03:00:00\" to=\"2016-09-11T04:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T03:00:00 to 2016-09-11T04:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T03:00:00  -->\n" +
                "<windDirection deg=\"159.5\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"3.5\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1012.8\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T04:00:00\" to=\"2016-09-11T05:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T04:00:00 to 2016-09-11T05:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T04:00:00  -->\n" +
                "<windDirection deg=\"158.1\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"3.6\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1012.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T05:00:00\" to=\"2016-09-11T06:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T05:00:00 to 2016-09-11T06:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T05:00:00  -->\n" +
                "<windDirection deg=\"168.5\" code=\"SSE\" name=\"South-southeast\"/>\n" +
                "<windSpeed mps=\"3.4\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1011.8\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T06:00:00\" to=\"2016-09-11T07:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T06:00:00 to 2016-09-11T07:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T06:00:00  -->\n" +
                "<windDirection deg=\"171.9\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"4.0\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1011.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T07:00:00\" to=\"2016-09-11T08:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T07:00:00 to 2016-09-11T08:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T07:00:00  -->\n" +
                "<windDirection deg=\"181.8\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"4.3\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1011.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T08:00:00\" to=\"2016-09-11T09:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T08:00:00 to 2016-09-11T09:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T08:00:00  -->\n" +
                "<windDirection deg=\"183.2\" code=\"S\" name=\"South\"/>\n" +
                "<windSpeed mps=\"4.5\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1011.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T09:00:00\" to=\"2016-09-11T10:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T09:00:00 to 2016-09-11T10:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T09:00:00  -->\n" +
                "<windDirection deg=\"200.5\" code=\"SSW\" name=\"South-southwest\"/>\n" +
                "<windSpeed mps=\"4.8\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1011.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T10:00:00\" to=\"2016-09-11T11:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T10:00:00 to 2016-09-11T11:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T10:00:00  -->\n" +
                "<windDirection deg=\"215.6\" code=\"SW\" name=\"Southwest\"/>\n" +
                "<windSpeed mps=\"4.0\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1012.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T11:00:00\" to=\"2016-09-11T12:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T11:00:00 to 2016-09-11T12:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T11:00:00  -->\n" +
                "<windDirection deg=\"233.0\" code=\"SW\" name=\"Southwest\"/>\n" +
                "<windSpeed mps=\"4.9\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"20\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1012.8\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T12:00:00\" to=\"2016-09-11T13:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T12:00:00 to 2016-09-11T13:00:00 \n" +
                "-->\n" +
                "<symbol number=\"4\" numberEx=\"4\" name=\"Cloudy\" var=\"04\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T12:00:00  -->\n" +
                "<windDirection deg=\"253.8\" code=\"WSW\" name=\"West-southwest\"/>\n" +
                "<windSpeed mps=\"4.4\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1013.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T13:00:00\" to=\"2016-09-11T14:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T13:00:00 to 2016-09-11T14:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T13:00:00  -->\n" +
                "<windDirection deg=\"262.0\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"4.8\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"19\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1014.4\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T14:00:00\" to=\"2016-09-11T15:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T14:00:00 to 2016-09-11T15:00:00 \n" +
                "-->\n" +
                "<symbol number=\"2\" numberEx=\"2\" name=\"Fair\" var=\"02d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T14:00:00  -->\n" +
                "<windDirection deg=\"269.1\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"4.9\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1015.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T15:00:00\" to=\"2016-09-11T16:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T15:00:00 to 2016-09-11T16:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"01d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T15:00:00  -->\n" +
                "<windDirection deg=\"271.4\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"5.1\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1016.7\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T16:00:00\" to=\"2016-09-11T17:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T16:00:00 to 2016-09-11T17:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T16:00:00  -->\n" +
                "<windDirection deg=\"270.7\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"4.8\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"18\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1017.2\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T17:00:00\" to=\"2016-09-11T18:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T17:00:00 to 2016-09-11T18:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T17:00:00  -->\n" +
                "<windDirection deg=\"271.5\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"4.7\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1017.6\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T18:00:00\" to=\"2016-09-11T19:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T18:00:00 to 2016-09-11T19:00:00 \n" +
                "-->\n" +
                "<symbol number=\"2\" numberEx=\"2\" name=\"Fair\" var=\"02d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T18:00:00  -->\n" +
                "<windDirection deg=\"271.1\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"4.2\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"17\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1017.9\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T19:00:00\" to=\"2016-09-11T20:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T19:00:00 to 2016-09-11T20:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"03d\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T19:00:00  -->\n" +
                "<windDirection deg=\"271.5\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"3.8\" name=\"Gentle breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"16\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1018.3\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T20:00:00\" to=\"2016-09-11T21:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T20:00:00 to 2016-09-11T21:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T20:00:00  -->\n" +
                "<windDirection deg=\"265.5\" code=\"W\" name=\"West\"/>\n" +
                "<windSpeed mps=\"3.1\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"16\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1018.8\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T21:00:00\" to=\"2016-09-11T22:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T21:00:00 to 2016-09-11T22:00:00 \n" +
                "-->\n" +
                "<symbol number=\"3\" numberEx=\"3\" name=\"Partly cloudy\" var=\"mf/03n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T21:00:00  -->\n" +
                "<windDirection deg=\"253.7\" code=\"WSW\" name=\"West-southwest\"/>\n" +
                "<windSpeed mps=\"2.6\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"15\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1019.1\"/>\n" +
                "</time>\n" +
                "<time from=\"2016-09-11T22:00:00\" to=\"2016-09-11T23:00:00\">\n" +
                "<!--\n" +
                " Valid from 2016-09-11T22:00:00 to 2016-09-11T23:00:00 \n" +
                "-->\n" +
                "<symbol number=\"1\" numberEx=\"1\" name=\"Clear sky\" var=\"mf/01n.31\"/>\n" +
                "<precipitation value=\"0\"/>\n" +
                "<!--  Valid at 2016-09-11T22:00:00  -->\n" +
                "<windDirection deg=\"241.3\" code=\"WSW\" name=\"West-southwest\"/>\n" +
                "<windSpeed mps=\"1.9\" name=\"Light breeze\"/>\n" +
                "<temperature unit=\"celsius\" value=\"14\"/>\n" +
                "<pressure unit=\"hPa\" value=\"1019.2\"/>\n" +
                "</time>\n" +
                "</tabular>\n" +
                "</forecast>\n" +
                "</weatherdata>";
    }
}