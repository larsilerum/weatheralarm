package com.squeed.numbergame.service;

import com.squeed.numbergame.entity.Time;
import com.squeed.numbergame.entity.WeatherData;
import com.squeed.numbergame.integration.YrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class WeatherAlarmService {

    private final Logger log = Logger.getLogger(getClass().getName());

    @Autowired
    private YrClient yrClient;

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        log.info("Getting weather forecast");

        try {
            WeatherData weatherData = yrClient.getForecast();
            checkAlarms(weatherData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkAlarms(WeatherData weatherData) {
        Time time = weatherData.getForecast().getTabular().get(11);
        if (time.getTemperature().getValue() < 25 ) {
            System.out.println("time.getTemperature().getValue() = " + time.getTemperature().getValue());
        }
    }
}
