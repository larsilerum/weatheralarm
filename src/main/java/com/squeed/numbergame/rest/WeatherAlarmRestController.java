package com.squeed.numbergame.rest;

import com.squeed.numbergame.entity.WeatherData;
import com.squeed.numbergame.integration.YrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@RestController
@RequestMapping("/weather")
public class WeatherAlarmRestController {

    private final Logger log = Logger.getLogger(getClass().getName());

    private static final int MINVALUE = 1;
    private static final int MAXVALUE = 100;

    public static Map<String, Map<String, Integer>> gameMap = new HashMap<>();
    public static Map<String, Integer> resultMap = new HashMap<>();

    @Autowired
    private YrClient yrClient;

    @RequestMapping(path = "/ping", produces = "text/plain")
    public String ping() {
        return "pong";
    }

    @RequestMapping(path = "/forecast", method = RequestMethod.GET)
    @ResponseBody
    public WeatherData getForecast() {
        try {
            WeatherData weatherData = yrClient.getForecast();
        } catch (Exception e) {
            log.warning("Failed to get forecast");
        }
        return new WeatherData();
    }



//    @RequestMapping(method = RequestMethod.POST)
//    @ResponseBody
//    public void postRequest(@RequestBody GamePayload req) {
//
//        if (gameMap.containsKey(req.getGameName())) {
//            log.warning(String.format("Game: %s - Already exists", req.getGameName()));
//            return;
//        }
//
//        log.info(String.format("Game: %s - Created new game", req.getGameName()));
//
//        int result = MINVALUE + (int) (Math.random() * MAXVALUE);
//        log.info(String.format("Game: %s - Randomizing answer: %d", req.getGameName(), result));
//
//        Map<String, Integer> players = new HashMap<>();
//
//        req.getPlayers().stream().forEach((playerName) -> {
//            log.info(String.format("Game: %s - Added player: %s", req.getGameName(), playerName));
//
//            players.put(playerName, null);
//        });
//
//        long totalPlayers = players.keySet().stream().count();
//        log.info(String.format("Game: %s - Total players: %d", req.getGameName(), totalPlayers));
//
//        gameMap.put(req.getGameName(), players);
//        resultMap.put(req.getGameName(), result);
//    }
//
//    @RequestMapping(path = "/questions", method = RequestMethod.GET)
//    @ResponseBody
//    public String getQuestion() {
//        return String.format("Guess the number (%d-%d)", MINVALUE, MAXVALUE);
//    }

//    @RequestMapping(path = "/{gameName}/answers", method = RequestMethod.POST)
//    @ResponseBody
//    public void postRequest(@PathVariable String gameName, @RequestBody AnswerPayload req) {
//        log.info(String.format("Game: %s - Player: %s responded: %d", gameName, req.getPlayerName(), req.getAnswer()));
//
//        Map<String, Integer> playerAnswers = gameMap.get(gameName);
//
//        if (playerAnswers != null) {
//            long totalPlayers = playerAnswers.keySet().stream().count();
//
//            if (playerAnswers.containsKey(req.getPlayerName())) {
//
//                if (playerAnswers.get(req.getPlayerName()) != null) {
//                    log.warning(String.format("Game: %s - Player: %s har already responded", gameName, req.getPlayerName()));
//                    return;
//                }
//
//                playerAnswers.put(req.getPlayerName(), req.getAnswer());
//
//                long totalAnswers = playerAnswers.values().stream().filter(v -> v != null).count();
//                log.info(String.format("Game: %s - Total answers: %d/%d", gameName, totalAnswers, totalPlayers));
//
//                if (totalPlayers == totalAnswers) {
//                    log.info(String.format("Game: %s - All answers logged", gameName));
//                    log.info(String.format("Game: %s - Calculating results", gameName));
//                    ScorePayload scores = ResultCalculator.calculateResult(playerAnswers, resultMap.get(gameName));
//                    log.info(String.format("Game: %s - Posting scores", gameName));
//                    yrClient.registerScores(gameName, scores);
//                }
//            } else {
//                log.warning(String.format("Game: %s - Player: %s does not exist", gameName, req.getPlayerName()));
//            }
//        } else {
//            log.warning(String.format("Game: %s - Does not exist", gameName));
//        }
//
//    }


}
