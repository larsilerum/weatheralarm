package com.squeed.numbergame.integration;

import java.util.logging.Logger;

import com.squeed.numbergame.entity.WeatherData;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
public class YrClient {

    private final Logger log = Logger.getLogger(getClass().getName());

    private static final String YRURL = "http://www.yr.no/place/Sweden/V%C3%A4stra_G%C3%B6taland/Lerum/forecast_hour_by_hour.xml";
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int READ_TIMEOUT = 5000;

    public WeatherData getForecast() throws Exception {
        Client client = getClient();
        Response response = client.target(YRURL).request().accept(MediaType.TEXT_XML_TYPE).get();
        String xml = response.readEntity(String.class);
        Serializer serializer = new Persister();
        WeatherData weatherData = serializer.read(WeatherData.class, xml);
        return weatherData;

    }

    private Client getClient() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT);
        configuration.property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT);
        return ClientBuilder.newClient(configuration);
    }
}
