package com.squeed.numbergame.entity;


import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Root;

@Root(strict = false)
@Default(DefaultType.FIELD)
public class Time {

    private Temperature temperature;
    private WindSpeed windSpeed;

    public Temperature getTemperature() {
        return temperature;
    }

    public WindSpeed getWindSpeed() {
        return windSpeed;
    }
}
