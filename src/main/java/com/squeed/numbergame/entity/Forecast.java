package com.squeed.numbergame.entity;


import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict=false)
@Default(DefaultType.FIELD)
public class Forecast {
    private List<Time> tabular;

    public List<Time> getTabular() {
        return tabular;
    }

    public void setTabular(List<Time> tabular) {
        this.tabular = tabular;
    }
}
