package com.squeed.numbergame.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class WindSpeed {

    @Attribute
    private String mps;

    public String getMps() {
        return mps;
    }

    public void setMps(String mps) {
        this.mps = mps;
    }
}
