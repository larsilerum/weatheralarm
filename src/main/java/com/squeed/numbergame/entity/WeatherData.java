package com.squeed.numbergame.entity;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Root;

@Root(strict=false)
@Default(DefaultType.FIELD)
public class WeatherData {
    private Location location;
    private Forecast forecast;

    public Forecast getForecast() {
        return forecast;
    }
}
