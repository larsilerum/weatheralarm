package com.squeed.numbergame.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Root;

@Root
public class Temperature {

    @Attribute
    private String unit;

    @Attribute
    private String value;

    public Float getValue() {
        return Float.valueOf(value);
    }
}
